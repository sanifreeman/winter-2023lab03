public class Application{
    public static void main(String[]args){
        Student naruto=new Student();
        // creating student variable aka object
        Student aAron= new Student();
        naruto.age=19;
        //defining whats contents this student variable (age,LN,WG)
        naruto.lastName="Uzimaki";
        naruto.worstGrade=10;
        aAron.age=17;
        aAron.worstGrade=50;
        aAron.lastName="yager";
        System.out.println(naruto.age);
        System.out.println(naruto.lastName);
        System.out.println(naruto.worstGrade);
        System.out.println(aAron.age);
        System.out.println(aAron.lastName);
        System.out.println(aAron.worstGrade);
        naruto.theage();
        aAron.theage();
        Student[] section3= new Student[3];
        section3[0]=naruto;
        //so when ur here ur calling the contents of this variable in this ex (age)
        section3[1]=aAron;
        section3[2]=new Student();
        section3[2].age=20;
        section3[2].lastName="Freeman";
        section3[2].worstGrade=80;

        System.out.println(section3[0].age);
        System.out.println(section3[2].age);
        System.out.println(section3[2].lastName);
        System.out.println(section3[2].worstGrade);



    }
}